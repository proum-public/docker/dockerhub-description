#!/usr/bin/env sh
set -eo pipefail

IFS=$'\n\t'
SCRIPT_DIR="$(cd "$(dirname "$0")" ; pwd -P)"
PATH=$PATH:${SCRIPT_DIR}

# check prerequisites
for cmd in curl jq
do
    command -v ${cmd} > /dev/null || {  echo >&2 "${cmd} must be installed - exiting..."; exit 1; }
done

function usage() {
  echo "Usage: $0 [OPTIONS]"
  echo ""
  echo "Push files (like README.md) as descriptions to hub.docker.com"
  echo ""
  echo "        -u --dockerhub-username:     Username of the hub.docker.com login (either this or the ENV variable DOCKERHUB_USERNAME is required)"
  echo "        -p --dockerhub-password:     Password of the hub.docker.com login (either this or the ENV variable DOCKERHUB_PASSWORD is required)"
  echo "        -r --dockerhub-repository:   URL of the hub.docker.com repository to which the file will be uploaded (either this or the ENV variable DOCKERHUB_REPOSITORY is required)"
  echo "        -f --description-file:       Path to the file which will be uploaded (default: README.md) (either this or the ENV variable DESCRIPTION_FILE is required)"
  echo ""
  echo "Environment variables:"
  echo ""
  echo "        DOCKERHUB_USERNAME           Username of the hub.docker.com login"
  echo "        DOCKERHUB_PASSWORD           Password of the hub.docker.com login"
  echo "        DOCKERHUB_REPOSITORY         URL of the hub.docker.com repository to which the file will be uploaded"
  echo "        DESCRIPTION_FILE             Path to the file which will be uploaded (default: README.md)"
}

while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
        --dockerhub-username|-u)
        export DOCKERHUB_USERNAME="$2"
        shift
        shift
        ;;
        --dockerhub-password|-p)
        export DOCKERHUB_PASSWORD="$2"
        shift
        shift
        ;;
        --dockerhub-repository|-r)
        export DOCKERHUB_REPOSITORY="$2"
        shift
        shift
        ;;
        --description-file|-f)
        export DESCRIPTION_FILE="$2"
        shift
        shift
        ;;
        --help|-h|help)
        usage
        exit 0
        ;;
        *)
        shift
    esac
done

if [[ -z ${DOCKERHUB_USERNAME} ]]; then
  echo "DOCKERHUB_USERNAME variable not set!"
  usage
  exit 1
fi

if [[ -z ${DOCKERHUB_PASSWORD} ]]; then
  echo "DOCKERHUB_PASSWORD variable not set!"
  usage
  exit 1
fi

if [[ -z ${DOCKERHUB_REPOSITORY} ]]; then
  echo "DOCKERHUB_REPOSITORY variable not set!"
  usage
  exit 1
fi

if [[ -z ${DESCRIPTION_FILE} ]]; then
    if [[ -n "${CI_PROJECT_DIR}" ]]; then
      export DESCRIPTION_FILE="${CI_PROJECT_DIR}/README.md"
    else
      export DESCRIPTION_FILE="README.md"
    fi
fi

# Check if description files exists
if [[ ! -f ${DESCRIPTION_FILE} ]]; then
  echo "Couldn't find description file!"
  exit 1
fi

# Process includes
DESCRIPTION_FILE_MERGED="${DESCRIPTION_FILE}.merged"

# markdown_helper demands a git repository
# So if we are running in a Gitlab CI pipeline lets jump to the project folder
# otherwise create a dummy git project
if [[ -n "${CI_PROJECT_DIR}" ]]; then
  cd "${CI_PROJECT_DIR}"
else
  git init
fi

markdown_helper include --pristine "${DESCRIPTION_FILE}" "${DESCRIPTION_FILE_MERGED}"

# Check the file size
if [ $(wc -c <${DESCRIPTION_FILE_MERGED}) -gt 25000 ]; then
  echo "Description file size exceeds the maximum allowed 25000 bytes!"
  exit 1
fi

# URLS
LOGIN_URL="https://hub.docker.com/v2/users/login/"
REPO_URL="https://hub.docker.com/v2/repositories/${DOCKERHUB_REPOSITORY}/"

# Request API token from hub.docker.com
echo -n "Requesting API token... "
PAYLOAD=$(
  jq -n \
  --arg username "${DOCKERHUB_USERNAME}" \
  --arg password "${DOCKERHUB_PASSWORD}" \
  '{"username":$username,"password":$password}'
)

RESPONSE=$(
  curl -s \
  -X POST \
  -H 'Content-Type: application/json' \
  -d "${PAYLOAD}" \
  "${LOGIN_URL}"
)

# Check for an error (e.g. wrong credentials)
MSG=$(echo "${RESPONSE}" | jq 'select(.detail != null) | .detail')
if [[ -n "${MSG}" ]]
then
  echo "FAILED! (Error: ${MSG})"
  exit 1
fi

# Fetch token
TOKEN=$(echo "${RESPONSE}" | jq -r 'select(.token != null) | .token')
if [[ -n "${TOKEN}" ]]
then
  echo "OK!"
fi

# Send PATCH request to update description in repository and fetch response code
echo -n "Patching description of ${REPO_URL}... "
RESPONSE_CODE=$(
  curl \
  -s \
  --write-out %{response_code} \
   --output /dev/null \
  -X PATCH \
  -H "Authorization: JWT ${TOKEN}" \
  --data-urlencode "full_description@${DESCRIPTION_FILE_MERGED}" \
  "${REPO_URL}"
)

if [ ${RESPONSE_CODE} -eq 200 ]; then
  echo "OK!"
  exit 0
else
  echo "FAILED! (Error: ${RESPONSE_CODE})"
  exit 1
fi