FROM ruby:3.0-alpine

ARG MARKDOWN_HELPER_URL=https://github.com/BurdetteLamar/markdown_helper
ARG MARKDOWN_HELPER_VERSION=v2.5.4

LABEL \
  maintainer="Hauke Mettendorf <hauke.mettendorf@proum.de>" \
  org.opencontainers.image.title="dockerhub-description" \
  org.opencontainers.image.description="A simple alpine based container for pushing README.md files as descriptions to hub.docker.com in Gitlab CI pipelines" \
  org.opencontainers.image.authors="Hauke Mettendorf <hauke.mettendorf@proum.de>" \
  org.opencontainers.image.url="https://gitlab.com/proum-public/docker/dockerhub-description" \
  org.opencontainers.image.vendor="https://proum.de" \
  org.opencontainers.image.licenses="GNUv2"

RUN apk add -v --update --no-cache \
    curl \
    git \
    jq \
    patch

COPY patches /tmp/patches

RUN git clone ${MARKDOWN_HELPER_URL} \
    && cd markdown_helper \
    && git checkout ${MARKDOWN_HELPER_VERSION} \
    && patch markdown_helper.gemspec /tmp/patches/00-dependencies.patch \
    && rm Gemfile.lock \
    && bundler update \
    && rake install

COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]
